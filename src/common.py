# Built-in imports
from dataclasses import dataclass
from typing import Sequence

# Library imports
from z3 import (
    # Boolean data types
    And,
    Not,

    # Numeric data types
    IntNumRef,
    RatNumRef,

    # Solutions
    Implies,
    Solver,
    Z3Exception,
    sat,
    unsat,
)

# This is the common machinery used to process a given contract setup and learn if it is both self-consistent and safe.

##########################
# Constants
##########################
# These prefixes make outputs easier to group visually when reading reports.
INTENT_PREFIX = '[I]'
CONTRACT_PREFIX = '[C]'
REDEMPTION_PREFIX = '[R]'
EXECUTION_PREFIX = '[E]'


##########################
# Common elements for all validators
##########################
@dataclass(frozen=True)
class ContractConstraintsBaseClass:
    """Representation of guarantees and safety requirements that can be used to validate safety of a contract."""

    # Abstract methods to implement
    @classmethod
    def new(cls):
        # Using a new() constructor makes it easy to do custom setup when instantiating frozen dataclasses
        raise NotImplementedError

    @property
    def constant_guarantees(self) -> list:
        """Any named constants that are needed in other parts of this validator."""
        raise NotImplementedError

    @property
    def input_guarantees(self) -> list:
        """
        This part is the heart of the safety proofs.
        Once everything is working and your contract is safe, these are the rules that
        you must implement in a contract wrapper to ensure that contract users do not
        create contracts with dangerous parameters.
        """
        raise NotImplementedError

    @property
    def execution_guarantees(self) -> list:
        """
        These are the unchangeable constraints that emerge from execution of the contract.
        In other words, these relationships describe each step of the contract and allow the solver
        to translate any possible contract input into the same output that the VM will arrive at.
        """
        raise NotImplementedError

    @property
    def safety_requirements(self) -> list:
        """
        The minimal safety constraints that we want to guarantee for each step of execution including
        inputs, intermediate steps and outputs. The intermediate steps are a less obvious and equally
        important part of safety. Even if your inputs and outputs all look safe according to overall constraints,
        if any part of your contract triggers a VM error, then the process stops and you never get to the outputs.
        """
        raise NotImplementedError

    @property
    def complete_guarantees(self) -> list:
        """Combination of all guarantees that you commit to ensuring."""
        return [
            *self.constant_guarantees,
            *self.input_guarantees,
            *self.execution_guarantees,
        ]


def print_model(model):
    """Print a human-readable description of the contract model."""
    # Sort the items out a bit
    intents = []
    contract_parameters = []
    redemption = []
    execution = []
    other = []
    for parameter in model:
        if str(parameter).strip().startswith(INTENT_PREFIX):
            intents.append(parameter)
        elif str(parameter).strip().startswith(CONTRACT_PREFIX):
            contract_parameters.append(parameter)
        elif str(parameter).strip().startswith(REDEMPTION_PREFIX):
            redemption.append(parameter)
        elif str(parameter).strip().startswith(EXECUTION_PREFIX):
            execution.append(parameter)
        else:
            other.append(parameter)

    # Show the model values
    for parameter in intents + contract_parameters + redemption + execution + other:
        value = model[parameter]
        if isinstance(value, IntNumRef):
            print(f'{str(parameter):<35}: {model[parameter].as_long():>20}')
        elif isinstance(value, RatNumRef):
            print(f'{str(parameter):<35}: {model[parameter].as_decimal(2):>20}')
        else:
            print(f'{str(parameter):<35}: {model[parameter]}')


def describe_constraints(contract: ContractConstraintsBaseClass):
    """Print a human-readable description of the contract model's constraints."""
    print('Constant guarantees:')
    for g in contract.constant_guarantees:
        print(f'    {g}')
    print('Input guarantees:')
    for g in contract.input_guarantees:
        print(f'    {g}')
    print('Contract execution guarantees:')
    for g in contract.execution_guarantees:
        print(f'    {g}')
    print('Safety requirements:')
    for r in contract.safety_requirements:
        print(f'    {r}')


def assertions_are_self_consistent(assertions) -> bool:
    """Check if a set of assertions has internal conflicts that will confusingly always result in validation failure."""
    # Check if a set of assertions is self-consistent by asserting that some solution exists
    proof_solver = Solver()
    proof_solver.add(assertions)
    proof_result = proof_solver.check()

    # The only case that we are confident is when the solution is "sat". Other cases can mean various failures
    return proof_result == sat


def describe_safety(
        contract: ContractConstraintsBaseClass,
        should_describe_constraints: bool,
        should_show_redundancies: bool,
) -> None:
    """Print a human-readable description of whether the contract model's guarantees ensure its safety requirements."""
    # Get the source assertions to be worked with
    complete_guarantees = And(contract.complete_guarantees)
    safety_requirements = And(contract.safety_requirements)

    # Before relating them, ensure that the source assertions are self-consistent
    complete_guarantees_are_self_consistent = assertions_are_self_consistent(complete_guarantees)
    safety_requirements_are_self_consistent = assertions_are_self_consistent(safety_requirements)

    if not complete_guarantees_are_self_consistent:
        print('** WARNING: complete guarantees themselves are not self-consistent: not possible to create a valid input')
        for g in contract.complete_guarantees:
            print(f'    {g}')
    else:
        print('** Complete guarantees are self-consistent')

    if not safety_requirements_are_self_consistent:
        print('** WARNING: safety requirements themselves are not self-consistent: not possible to create a valid input')
        for s in contract.safety_requirements:
            print(f'    {s}')
    else:
        print('** Complete safety requirements are self-consistent')

    should_stop_early = (not complete_guarantees_are_self_consistent) or (not safety_requirements_are_self_consistent)
    if should_stop_early:
        return

    # Setup the key logical assertion between guarantees and safety
    safety_claim = Implies(
        a=complete_guarantees,
        b=safety_requirements,
    )

    # Check if the guarantees always imply the safety claim
    proof_solver = Solver()
    proof_solver.add(Not(safety_claim))
    proof_result = proof_solver.check()

    if proof_result == unsat:
        # If the negation is unsatisfiable, it means that as long as the guarantees are met,
        # there is no input that results in a violation of the safety claim.
        print(f'** SATISFIES SAFETY REQUIREMENTS')
        if should_describe_constraints:
            describe_constraints(contract)
        if should_show_redundancies:
            describe_redundancies(contract)
    else:
        # If the negation is not unsatisfiable (satisfiable or unknown), it means that there is some unknown issue or
        # that there exists some input that results in a violation of the safety claim.
        print(f'** FAILED - counter example:')
        describe_counter_example(
            proof_solver=proof_solver,
            complete_guarantees=contract.complete_guarantees,
            safety_requirements=contract.safety_requirements,
        )


def describe_redundancies(contract: ContractConstraintsBaseClass):
    """Print a human-readable description of any obvious redundancies in the model's guarantees. Not a perfect method, but it helps."""
    print('Guarantee Redundancies (may be none):')
    # Search for all guarantees related to main parameter of each guarantee
    related_guarantee_strings_by_parameter_name = {}
    for input_guarantee in contract.input_guarantees:
        main_parameter_name = str(input_guarantee).split()[0]
        if main_parameter_name in related_guarantee_strings_by_parameter_name:
            continue
        related_guarantee_strings_by_parameter_name[main_parameter_name] = [
            str(guarantee)
            for guarantee in contract.input_guarantees
            if main_parameter_name in str(guarantee)
        ]

    # Search for redundant guarantees
    redundant_guarantee_strings = []
    for i in range(len(contract.input_guarantees)):
        partial_input_guarantees = list(contract.input_guarantees)
        dropped = partial_input_guarantees.pop(i)
        sub_safety_claim = Implies(
            a=And(
                And(contract.constant_guarantees),
                And(partial_input_guarantees),
                And(contract.execution_guarantees)
            ),
            b=And(contract.safety_requirements),
        )
        redundancy_solver = Solver()
        redundancy_solver.add(Not(sub_safety_claim))
        sub_proof_result = redundancy_solver.check()
        if sub_proof_result == unsat:
            redundant_guarantee_string = str(dropped)
            redundant_guarantee_strings.append(redundant_guarantee_string)

            # Display a list of all directly related guarantees
            print(f'    {redundant_guarantee_string}    HAS REDUNDANCIES WITH:')
            main_parameter_name = redundant_guarantee_string.split()[0]
            for related_guarantee_string in related_guarantee_strings_by_parameter_name[main_parameter_name]:
                if related_guarantee_string != redundant_guarantee_string:
                    print(f'        {related_guarantee_string}')


def describe_counter_example(
        proof_solver,
        complete_guarantees: Sequence,
        safety_requirements: Sequence,
):
    """Print a human-readable description of a counter-example that demonstrates the model is not valid, and therefore not safe."""
    # Get a counter-example and display which parts of the safety claim are violated.
    counter_example = proof_solver.model()
    print_model(counter_example)

    all_constraints = list(complete_guarantees) + list(safety_requirements)

    print(f'violated constraints:')
    for constraint in all_constraints:
        if proof_solver.check(constraint) == unsat:
            print(f'  {constraint}')

    print(f'disqualified parameter values:')
    for parameter in counter_example:
        bad_parameter_solver = Solver()
        bad_parameter_solver.add(safety_requirements)

        try:
            potentially_violating_value = counter_example[parameter()]
        except Z3Exception as e:
            print(f'  (skipping parameter: {e})')
            continue

        # Check if the safety constraints are violated by the specific counterexample parameter
        bad_parameter_solver.add(parameter() == potentially_violating_value)
        if bad_parameter_solver.check() == unsat:
            print(f'  {parameter}: {potentially_violating_value}')
