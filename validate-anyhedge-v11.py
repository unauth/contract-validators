# Built-in imports
from dataclasses import dataclass

# Library imports
from z3 import (
    If,
    Int,
    Real,
    ToInt,
    ToReal,
    set_param,
)

# Local imports
from src.common import (
    INTENT_PREFIX, CONTRACT_PREFIX, REDEMPTION_PREFIX, EXECUTION_PREFIX,
    ContractConstraintsBaseClass,
    describe_safety,
)





##########################
#
# The content of this validator for the older v11 contract have not been updated in a long time.
# If you are looking for examples, I highly recommend you look at v12 instead.
#
##########################





##########################
# Configuration
##########################

# Remove width limit so printouts do not create multi-line messes
set_param(max_width=1000)
# set_param('parallel.enable', True)

# Switches
SHOULD_DESCRIBE_CONSTRAINTS = True
SHOULD_SHOW_REDUNDANCIES_FOR_BASE = True
SHOULD_SHOW_REDUNDANCIES_FOR_INTENT = False

##########################
# Lazy Constants
##########################

# Establishing constants as constraints lets us get names for them.
CONSTANT_GUARANTEES = []

# 32-Bit Integer Prices
MIN_PRICE = Int('MIN_PRICE')
MAX_PRICE = Int('MAX_PRICE')
CONSTANT_GUARANTEES.extend((
    MIN_PRICE == 1,
    MAX_PRICE == 2 ** 31 - 1,
))

# Dust
DUST = Int('DUST')
CONSTANT_GUARANTEES.extend((
    DUST == 546,
))

# Min/MAX VM Integers (with optional javascript Number limiting)
VM_MAX_INT = Int('VM_MAX_INT')
JAVASCRIPT_MAX_SAFE_INT = 2 ** 53 - 1
CONSTANT_GUARANTEES.extend((
    VM_MAX_INT == JAVASCRIPT_MAX_SAFE_INT,
))

# Conversion
SATS_PER_BCH = Int('SATS_PER_BCH')
CONSTANT_GUARANTEES.extend((
    SATS_PER_BCH == 100_000_000,
))

# Max Sats
# A very loose policy constraint ~100k BCH.
MAX_SATS = Int('MAX_SATS')
CONSTANT_GUARANTEES.extend((
    MAX_SATS == 100_000 * 100_000_000,
))


##########################
# Contract Constraints
##########################

# Note: This class now has a minimal set of non-redundant guarantees that satisfy safety requirements
@dataclass(frozen=True)
class AnyHedge_v0_11(ContractConstraintsBaseClass):
    # Intent Inputs
    start_price: Int

    # Creation Inputs
    high_liquidation_price: Int
    low_liquidation_price: Int
    nominal_composite: Int
    total_sats_in: Int

    # Redemption Inputs
    redeem_price: Int

    # Intermediate Execution Values
    _high_clamped_price: Int
    _clamped_price: Int
    _unsafe_short_sats_out: Int
    _unsafe_long_sats_out: Int

    # Final Execution Outputs
    short_sats_out: Int
    long_sats_out: Int

    @classmethod
    def new(cls):
        return cls(
            start_price=Int(f'{INTENT_PREFIX}start_price'),

            # Creation Inputs
            high_liquidation_price=Int(f'{CONTRACT_PREFIX}high_liquidation_price'),
            low_liquidation_price=Int(f'{CONTRACT_PREFIX}low_liquidation_price'),
            nominal_composite=Int(f'{CONTRACT_PREFIX}nominal_composite'),
            total_sats_in=Int(f'{CONTRACT_PREFIX}total_sats_in'),

            # Redemption Inputs
            redeem_price=Int(f'{REDEMPTION_PREFIX}redeem_price'),

            # Intermediate Execution Values
            _high_clamped_price=Int(f'{EXECUTION_PREFIX}high_clamped_price'),
            _clamped_price=Int(f'{EXECUTION_PREFIX}clamped_price'),
            _unsafe_short_sats_out=Int(f'{EXECUTION_PREFIX}unsafe_short_sats_out'),
            _unsafe_long_sats_out=Int(f'{EXECUTION_PREFIX}unsafe_long_sats_out'),

            # Final Execution Outputs
            short_sats_out=Int(f'{EXECUTION_PREFIX}short_sats_out'),
            long_sats_out=Int(f'{EXECUTION_PREFIX}long_sats_out'),
        )

    @property
    def constant_guarantees(self) -> list:
        return list(CONSTANT_GUARANTEES)

    @property
    def input_guarantees(self) -> list:
        return [
            # Basic value constraints
            # Note that this group might look redundant with constraints in base contract safety,
            # but they are on opposite sides of the proof line. These are guarantees. The other side is requirements.

            # self.start_price >= MIN_PRICE,  # redundant with [low_liquidation_price < start_price]
            # self.start_price <= MAX_PRICE,  # redundant with [high_liquidation_price > start_price]

            # self.high_liquidation_price >= MIN_PRICE,  # redundant with [high_liquidation_price > start_price]
            self.high_liquidation_price <= MAX_PRICE,

            self.low_liquidation_price >= MIN_PRICE,
            # self.low_liquidation_price <= MAX_PRICE,  # redundant with [low_liquidation_price < start_price]

            self.nominal_composite >= SATS_PER_BCH,
            self.nominal_composite <= VM_MAX_INT,

            self.total_sats_in >= DUST,
            self.total_sats_in <= MAX_SATS,

            self.redeem_price >= MIN_PRICE,
            self.redeem_price <= MAX_PRICE,

            # Relationship Constraints

            # Disallow instant liquidation by policy
            # long liquidation price < start price < short liquidation price
            self.low_liquidation_price < self.start_price,
            self.high_liquidation_price > self.start_price,

            # Ensure downstream safety requirements through careful back calculations that preserve exact integer math relationships

            # Ensure safety requirement [_unsafe_short_sats_out >= 0]
            #   min value of _unsafe_short_sats_out >= 0
            #   min value of nominal_composite / _clamped_price >= 0
            #   nominal_composite / high_liquidation_price >= 0
            #   nominal_composite >= 0  (stronger assertion already exists above)

            # Ensure safety requirement [_unsafe_short_sats_out <= MAX_SATS]
            #   max value of _unsafe_short_sats_out <= MAX_SATS
            #   max value of nominal_composite / _clamped_price <= MAX_SATS
            #   nominal_composite / low_liquidation_price <= MAX_SATS
            # self.nominal_composite / self.low_liquidation_price <= MAX_SATS,  # redundant with total sats below

            # Ensure safety requirement [_unsafe_long_sats_out >= 0]
            #   min value of _unsafe_long_sats_out >= 0
            #   min value of total_sats_in - short_sats_out >= 0
            #   min value of total_sats_in - max(DUST, _unsafe_short_sats_out) >= 0
            #   total_sats_in - (max value of max(DUST, _unsafe_short_sats_out)) >= 0
            #   total_sats_in - (max value of _unsafe_short_sats_out) >= 0
            #   total_sats_in - (max value of nominal_composite / _clamped_price) >= 0
            #   total_sats_in - (nominal_composite / low_liquidation_price) >= 0
            self.total_sats_in - (self.nominal_composite / self.low_liquidation_price) >= 0,

            # Ensure safety requirement [_unsafe_long_sats_out <= MAX_SATS]
            #   max value of _unsafe_long_sats_out <= MAX_SATS
            #   max value of total_sats_in - short_sats_out <= MAX_SATS
            #   max value of total_sats_in - max(DUST, _unsafe_short_sats_out) >= 0
            #   total_sats_in - (min value of max(DUST, _unsafe_short_sats_out)) >= 0
            #   total_sats_in - DUST >= 0 (already directly asserted above as total >= DUST)

            # Ensure safety requirement [short_sats_out >= DUST]
            #   min value of short_sats_out >= DUST
            #   min value of max(DUST, _unsafe_short_sats_out) >= DUST
            #   DUST >= DUST (trivially true)

            # Ensure safety requirement [short_sats_out <= MAX_SATS]
            #   max value of short_sats_out <= MAX_SATS
            #   max value of max(DUST, _unsafe_short_sats_out) <= MAX_SATS
            #   max value of _unsafe_short_sats_out <= MAX_SATS (already directly asserted above)

            # Ensure safety requirement [long_sats_out >= DUST]
            #   min value of long_sats_out >= DUST
            #   min value of max(DUST, _unsafe_long_sats_out) >= DUST
            #   DUST >= DUST (trivially true)

            # Ensure safety requirement [long_sats_out <= MAX_SATS]
            #   max value of long_sats_out <= MAX_SATS
            #   max value of max(DUST, _unsafe_long_sats_out) <= MAX_SATS
            #   max value of _unsafe_long_sats_out <= MAX_SATS (already directly asserted above)
        ]

    @property
    def execution_guarantees(self) -> list:
        """The unchangeable constraints that emerge from execution of the contract."""
        return [
            # equivalent: min(redeem_price, high_liquidation_price)
            self._high_clamped_price == If(self.redeem_price < self.high_liquidation_price, self.redeem_price, self.high_liquidation_price),

            # equivalent: max(highClampedOraclePrice, lowLiquidationPrice)
            self._clamped_price == If(self._high_clamped_price > self.low_liquidation_price, self._high_clamped_price, self.low_liquidation_price),

            # This is integer mathematics and results in a div mod
            self._unsafe_short_sats_out == self.nominal_composite / self._clamped_price,
            self.short_sats_out == If(self._unsafe_short_sats_out > DUST, self._unsafe_short_sats_out, DUST),

            self._unsafe_long_sats_out == self.total_sats_in - self.short_sats_out,
            self.long_sats_out == If(self._unsafe_long_sats_out > DUST, self._unsafe_long_sats_out, DUST),
        ]

    @property
    def safety_requirements(self) -> list:
        """The minimal safety constraints that we want to guarantee for each step of execution."""
        return [
            # Contract Parameters
            self.high_liquidation_price >= MIN_PRICE,
            self.high_liquidation_price <= MAX_PRICE,

            self.low_liquidation_price >= MIN_PRICE,
            self.low_liquidation_price <= MAX_PRICE,

            # Enforce anti-instant liquidation as a safety policy
            self.low_liquidation_price < self.start_price,
            self.high_liquidation_price > self.start_price,

            self.nominal_composite >= SATS_PER_BCH,
            self.nominal_composite <= VM_MAX_INT,

            self.total_sats_in >= DUST,
            self.total_sats_in <= MAX_SATS,

            # Execution
            self.redeem_price >= MIN_PRICE,
            self.redeem_price <= MAX_PRICE,

            self._high_clamped_price >= MIN_PRICE,
            self._high_clamped_price <= MAX_PRICE,

            self._clamped_price >= MIN_PRICE,
            self._clamped_price <= MAX_PRICE,

            self._unsafe_short_sats_out >= 0,
            self._unsafe_short_sats_out <= MAX_SATS,

            self._unsafe_long_sats_out >= 0,
            self._unsafe_long_sats_out <= MAX_SATS,

            # Final Outputs
            self.short_sats_out >= DUST,
            self.short_sats_out <= MAX_SATS,

            self.long_sats_out >= DUST,
            self.long_sats_out <= MAX_SATS,
        ]


@dataclass(frozen=True)
class AnyHedge_v0_11_ByIntent(ContractConstraintsBaseClass):
    # Intent-based approach:
    # The approach is an alternative to a backwards derivation of boundaries back to intent values
    # because that is fraught with integer math issues, special cases and is a pain in the ass.
    # The approach is to establish intent constraints that may have a minimal set of post-constraints
    # on the resulting base parameters (taken from the base contract safety parameters) in order to
    # fulfill the safety requirements. The reason for wanting a minimal set is that any results caught
    # only at the parameter level requires computation instead of having an a priori set of
    # boundaries to follow.

    base_contract: AnyHedge_v0_11

    # Intent Inputs
    nominal_hedge_units: Real
    low_liquidation_price_multiplier: Real
    high_liquidation_price_multiplier: Real

    # Intent to Contract Intermediates
    _hedge_sats_in: Int
    _long_sats_in: Int

    @classmethod
    def new(cls):
        return cls(
            base_contract=AnyHedge_v0_11.new(),

            # Intent Inputs
            nominal_hedge_units=Real(f'{INTENT_PREFIX}nominal_hedge_units'),
            low_liquidation_price_multiplier=Real(f'{INTENT_PREFIX}low_liquidation_price_multiplier'),
            high_liquidation_price_multiplier=Real(f'{INTENT_PREFIX}high_liquidation_price_multiplier'),

            # Intent to Contract Intermediates
            _hedge_sats_in=Int(f'{INTENT_PREFIX}_hedge_sats_in'),
            _long_sats_in=Int(f'{INTENT_PREFIX}_long_sats_in'),
        )

    @property
    def constant_guarantees(self) -> list:
        return self.base_contract.constant_guarantees

    @property
    def input_guarantees(self) -> list:
        # Establish the boundaries that must be enforced at step 1 of making an intent.
        # The concept is that if we can find a set of boundaries here that results in safe
        # contracts, then we have a low cost way to be confident about contract safety.

        # Note that we do not extend the base in this case. The input guarantees are the
        # top level guarantees and shouldn't depend on guarantees that require calculation
        # to reach and verify.

        # get a short name for readability
        base = self.base_contract

        intent_input_guarantees = [
            # Intent Parameter
            # start_price and redeem_price are on the base, but they are inputs for input guarantee purposes
            # base.start_price > MIN_PRICE,  # redundant with no-instant settlement requirement below
            # base.start_price < MAX_PRICE,  # redundant with no-instant settlement requirement below
            base.redeem_price >= MIN_PRICE,
            base.redeem_price <= MAX_PRICE,

            # Intent Parameter
            # Note: This value could be 1, but using a larger value helps to avoid non-valuable edges.
            self.nominal_hedge_units >= 1,
            self.nominal_hedge_units <= 900_000_00,

            # Intent Parameter
            # Enforce the no-instant settlement by back-calculation here, using a gap of 2 and integer math for simplicity
            # start_price * x <= start_price - 2
            # x <= (start_price - 2) / start_price
            self.low_liquidation_price_multiplier <= (ToReal(base.start_price) - 2) / ToReal(base.start_price),
            self.low_liquidation_price_multiplier >= 0.01,

            # Intent Parameter
            # Enforce the no-instant settlement by back-calculation here, using a gap of 2 and integer math for simplicity
            # start_price * x >= start_price + 2
            # x >= (start_price + 2) / start_price
            self.high_liquidation_price_multiplier >= (ToReal(base.start_price) + 2) / ToReal(base.start_price),
            self.high_liquidation_price_multiplier <= 101,
        ]

        # The intent parameter constraints as currently stated are not sufficient to ensure contract safety.
        # Here some downstream constraints are injected into the set in order to ensure safety.
        # If possible, these should be back-propagated to intent.
        additional_contract_parameter_guarantees = [
            # We drag in an additional value that is cheap to calculate, to avoid non-valuable edges.
            base.low_liquidation_price >= 100,
            base.low_liquidation_price < base.start_price,

            base.high_liquidation_price > base.start_price,
            base.high_liquidation_price <= MAX_PRICE,

            # With easy calculation we can confirm if these are true or not.
            self._hedge_sats_in >= DUST,
            self._long_sats_in >= DUST,
            base.total_sats_in <= MAX_SATS,

            # Constraints below are remaining constraints from base input guarantees that we didn't eliminate or use yet.

            # base.nominal_composite <= VM_MAX_INT,

            # Ensure safety requirement [_unsafe_short_sats_out >= 0]
            #   min value of _unsafe_short_sats_out >= 0
            #   min value of nominal_composite / _clamped_price >= 0
            #   nominal_composite / high_liquidation_price >= 0
            #   nominal_composite >= 0  (stronger assertion already exists above)

            # Ensure safety requirement [_unsafe_short_sats_out <= MAX_SATS]
            #   max value of _unsafe_short_sats_out <= MAX_SATS
            #   max value of nominal_composite / _clamped_price <= MAX_SATS
            #   nominal_composite / low_liquidation_price <= MAX_SATS
            # self.nominal_composite / self.low_liquidation_price <= MAX_SATS,  # redundant with total sats below

            # Ensure safety requirement [_unsafe_long_sats_out >= 0]
            #   min value of _unsafe_long_sats_out >= 0
            #   min value of total_sats_in - short_sats_out >= 0
            #   min value of total_sats_in - max(DUST, _unsafe_short_sats_out) >= 0
            #   total_sats_in - (max value of max(DUST, _unsafe_short_sats_out)) >= 0
            #   total_sats_in - (max value of _unsafe_short_sats_out) >= 0
            #   total_sats_in - (max value of nominal_composite / _clamped_price) >= 0
            #   total_sats_in - (nominal_composite / low_liquidation_price) >= 0
            # base.total_sats_in - (base.nominal_composite / base.low_liquidation_price) >= 0,
        ]

        combined_input_guarantees = intent_input_guarantees + additional_contract_parameter_guarantees
        return combined_input_guarantees

    @property
    def execution_guarantees(self) -> list:
        # Take the existing execution steps and extend them back to include any calculations that happen
        # when transforming intent parameters to contract parameters.

        # get a short name for readability
        base = self.base_contract

        intent_to_contract_execution_guarantees = [
            # Contract Parameter
            # Note: +0.5 is the integer math equivalent of round_half_up
            base.nominal_composite == ToInt(0.5 + self.nominal_hedge_units * ToReal(SATS_PER_BCH)),

            # Contract Parameter
            # Note: This uses real division until the conversion to integer
            # Note: Truncating with +0.5 is the integer math equivalent of round_half_up
            base.high_liquidation_price == ToInt(0.5 + ToReal(base.start_price) * self.high_liquidation_price_multiplier),

            # Contract Parameter
            # Note: Truncating with +0.5 is the integer math equivalent of round_half_up
            base.low_liquidation_price == ToInt(0.5 + ToReal(base.start_price) * self.low_liquidation_price_multiplier),

            # Intermediate
            # Note: Truncating with +0.5 is the integer math equivalent of round_half_up
            self._hedge_sats_in == ToInt(0.5 + ((self.nominal_hedge_units * ToReal(SATS_PER_BCH)) / ToReal(base.start_price))),

            # Contract Parameter
            # Note: Truncating with +0.5 is the integer math equivalent of round_half_up
            base.total_sats_in == ToInt(0.5 + ((self.nominal_hedge_units * ToReal(SATS_PER_BCH)) / ToReal(base.low_liquidation_price))),

            # Intermediate: long sats in
            self._long_sats_in == base.total_sats_in - self._hedge_sats_in,
        ]

        combined_execution_guarantees = intent_to_contract_execution_guarantees + base.execution_guarantees
        return combined_execution_guarantees

    @property
    def safety_requirements(self) -> list:
        # Take the existing safety requirements and extend them with safety requirements
        # for the new intent and intermediate values
        intent_safety_requirements = [
            # Simple positive requirement for nominal hedge safety
            self.nominal_hedge_units >= 1,

            # simple range boundaries for price multipliers
            self.low_liquidation_price_multiplier > 0,
            self.low_liquidation_price_multiplier < 1,
            self.high_liquidation_price_multiplier > 1,

            # simple vm positive integer boundaries for intermediate sat values
            self._hedge_sats_in >= 1,
            self._hedge_sats_in <= MAX_SATS,
            self._long_sats_in >= 1,
            self._long_sats_in <= MAX_SATS,
        ]

        # Combine with the existing safety requirements
        combined_safety_requirements = self.base_contract.safety_requirements + intent_safety_requirements

        return combined_safety_requirements


##########################
# Main
##########################
def main():
    print('Checking safety of: original AnyHedge base contract parameters')
    describe_safety(
        contract=AnyHedge_v0_11.new(),
        should_describe_constraints=SHOULD_DESCRIBE_CONSTRAINTS,
        should_show_redundancies=SHOULD_SHOW_REDUNDANCIES_FOR_BASE,
    )

    print('\n\nChecking safety of: original AnyHedge intent parameters')
    describe_safety(
        contract=AnyHedge_v0_11_ByIntent.new(),
        should_describe_constraints=SHOULD_DESCRIBE_CONSTRAINTS,
        should_show_redundancies=SHOULD_SHOW_REDUNDANCIES_FOR_INTENT,
    )


if __name__ == '__main__':
    main()
