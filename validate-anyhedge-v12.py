# Built-in imports
from dataclasses import dataclass

# Library imports
from z3 import (
    Bool,
    If,
    Int,
    Real,
    ToInt,
    ToReal,
    set_param,
)

# Local imports
from src.common import (
    INTENT_PREFIX, CONTRACT_PREFIX, REDEMPTION_PREFIX, EXECUTION_PREFIX,
    ContractConstraintsBaseClass,
    describe_safety,
)

# Look at the bottom of the file in main() to see how this all executes.

##########################
# Configuration
##########################

# Remove z3 width limit so printouts do not create multi-line messes
set_param(max_width=1000)
# set_param('parallel.enable', True)

# Enable this to print out constraints for reference
SHOULD_DESCRIBE_CONSTRAINTS = True

# Enable this to attempt to find redundancies in the primary constraints
# Remember, it's not necessarily a goal to have an perfectly non-redundant set of constraints.
# However, in your implementation, it may mean that you can't test some constraints since
# one or the other will usually trigger first.
SHOULD_SHOW_REDUNDANCIES_FOR_BASE_CONSTRAINTS = True

# Enable this to attempt to find redundancies in the intent constraints
# (Currently disabled because it takes forever)
SHOULD_SHOW_REDUNDANCIES_FOR_INTENT_CONSTRAINTS = False

##########################
# Named Constants (as Constraints)
##########################

# Establishing constants as trivial constraints lets us get names for them when z3 displays things.
CONSTANT_GUARANTEES = []

# 32-Bit Signed Integer Prices
# The oracle specification limits prices to 32 bit unsigned integers, so we establish that range.
MIN_PRICE = Int('MIN_PRICE')
MAX_PRICE = Int('MAX_PRICE')
CONSTANT_GUARANTEES.extend((
    MIN_PRICE == 1,
    MAX_PRICE == (2 ** 31) - 1,
))

# Dust
# The contract now uses a more conservative 1332 instead of common p2pkh 546.
# See discussion here: https://bitcoincashresearch.org/t/friday-night-challenge-worst-case-dust/1181
DUST = Int('DUST')
CONSTANT_GUARANTEES.extend((
    DUST == 1332,
))

# Min/MAX Signed 64-bit VM Integers
# The contract infrastructure is now all switched to BigInt on javascript, and strict custom
# serialization to ensure no accidental conversions to float by json parsers.
# Therefore, the limit is the actual VM limit instead of the javascript ~53-bit limit
# that was used previously. For example reference, that limit created a ~$900k limit on nominal hedge positions.
VM_MAX_INT = Int('VM_MAX_INT')
CONSTANT_GUARANTEES.extend((
    VM_MAX_INT == (2 ** 63) - 1,
))

# Fixed Unit Conversion
SATS_PER_BCH = Int('SATS_PER_BCH')
CONSTANT_GUARANTEES.extend((
    SATS_PER_BCH == 100_000_000,
))

# Max Sats
# A very loose policy constraint ~100k BCH.
MAX_SATS = Int('MAX_SATS')
CONSTANT_GUARANTEES.extend((
    MAX_SATS == 100_000 * 100_000_000,
))


##########################
# Contract Constraints
##########################

# Look at the generic ContractConstraintsBaseClass to see documentation of what all these parts mean.

@dataclass(frozen=True)
class AnyHedge_v0_12(ContractConstraintsBaseClass):
    """
    This class is the raw representation of an AnyHedge contract.
    "Raw" means that, there is not a layer of user-friendly nicety that we can validate - just the raw contract values.
    For validation of a user-friendly value layer, see the next class as an example of "Intent".
    """
    # Contract Creation: External Inputs
    start_price: Int

    # Contract Creation: Parameter Inputs
    high_liquidation_price: Int
    low_liquidation_price: Int
    nominal_composite: Int
    sats_for_nominal_units_at_high_liquidation: Int
    total_sats_in: Int

    # Contract Redemption: External Inputs
    redeem_price: Int

    # Contract Redemption: Intermediate Execution Values
    # These are prefixed with underscore (python's typical indicator for internal/private)
    # so that the focus stays on the primary inputs/outputs.
    _high_clamped_price: Int
    _clamped_price: Int
    _sats_for_nominal_units_at_redemption: Int
    _unsafe_short_sats_out: Int
    _unsafe_long_sats_out: Int

    # Contract Redemption: Transaction Outputs
    short_sats_out: Int
    long_sats_out: Int

    @classmethod
    def new(cls):
        return cls(
            # Below each variable we need to work with is assigned a name and type without specifying the value.
            # This allows the solver to assign whatever values it wants when doing proofs.

            # Contract Creation: External Inputs
            start_price=Int(f'{INTENT_PREFIX}start_price'),

            # Contract Creation: Parameter Inputs
            high_liquidation_price=Int(f'{CONTRACT_PREFIX}high_liquidation_price'),
            low_liquidation_price=Int(f'{CONTRACT_PREFIX}low_liquidation_price'),
            nominal_composite=Int(f'{CONTRACT_PREFIX}nominal_composite'),
            sats_for_nominal_units_at_high_liquidation=Int(f'{CONTRACT_PREFIX}sats_for_nominal_units_at_high_liquidation'),
            total_sats_in=Int(f'{CONTRACT_PREFIX}total_sats_in'),

            # Contract Redemption: External Inputs
            redeem_price=Int(f'{REDEMPTION_PREFIX}redeem_price'),

            # Contract Redemption: Intermediate Execution Values
            _high_clamped_price=Int(f'{EXECUTION_PREFIX}high_clamped_price'),
            _clamped_price=Int(f'{EXECUTION_PREFIX}clamped_price'),
            _sats_for_nominal_units_at_redemption=Int(f'{EXECUTION_PREFIX}sats_for_nominal_units_at_redemption'),
            _unsafe_short_sats_out=Int(f'{EXECUTION_PREFIX}unsafe_short_sats_out'),
            _unsafe_long_sats_out=Int(f'{EXECUTION_PREFIX}unsafe_long_sats_out'),

            # Contract Redemption: Transaction Outputs
            short_sats_out=Int(f'{EXECUTION_PREFIX}short_sats_out'),
            long_sats_out=Int(f'{EXECUTION_PREFIX}long_sats_out'),
        )

    @property
    def constant_guarantees(self) -> list:
        return list(CONSTANT_GUARANTEES)

    @property
    def input_guarantees(self) -> list:
        return [
            # Basic value constraints
            # Note that this group might look redundant with constraints in base contract safety,
            # but they are on opposite sides of the proof line. These are guarantees that *you* must
            # enforce. The other side is requirements that must be met for the contract to be safe.

            # Note that some constraints are commented out and include a reason explaining what other constraint they are redundant with.

            # self.start_price >= MIN_PRICE,  # redundant with [start_price > low_liquidation_price]
            # self.start_price <= MAX_PRICE,  # redundant with [start_price < high_liquidation_price]

            # self.high_liquidation_price >= MIN_PRICE,  # redundant with [high_liquidation_price > start_price]
            self.high_liquidation_price <= MAX_PRICE,

            self.low_liquidation_price >= MIN_PRICE,
            # self.low_liquidation_price <= MAX_PRICE,  # redundant with [low_liquidation_price < start_price]

            self.nominal_composite >= SATS_PER_BCH,
            self.nominal_composite <= VM_MAX_INT,

            self.sats_for_nominal_units_at_high_liquidation >= 0,
            # self.sats_for_nominal_units_at_high_liquidation <= MAX_SATS,  # redundant with [nominal_composite / high_liquidation_price - sats_for_nominal_units_at_high_liquidation >= 0]

            self.total_sats_in >= DUST,
            self.total_sats_in <= MAX_SATS,

            self.redeem_price >= MIN_PRICE,
            self.redeem_price <= MAX_PRICE,

            # Policy Constraints
            # Note that these are a gray area of rules that you want to enforce,
            # and they might make some other more basic constraints redundant.

            # Disallow instant liquidation by policy
            # long liquidation price < start price < short liquidation price
            self.low_liquidation_price < self.start_price,
            self.high_liquidation_price > self.start_price,

            # Back-calculated Safety Constraints
            # This is where things start to become a little complex and you need to think carefully.
            # In the end though, if you do something wrong here, the solver will notice and
            # show you where you went wrong. You also might make a wrong but harmless constraint
            # that doesn't impact safety.

            # Ensure safety requirement [_sats_for_nominal_units_at_redemption >= 1]
            #   _sats_for_nominal_units_at_redemption >= 1
            #   nominal_composite / _clamped_price >= 1
            # Apply the strictest future price that makes this condition hardest to meet (results in smallest expression value)
            #   nominal_composite / high_liquidation_price >= 1
            self.nominal_composite / self.high_liquidation_price >= 1,

            # Ensure safety requirement [_sats_for_nominal_units_at_redemption <= MAX_SATS]
            #   _sats_for_nominal_units_at_redemption <= MAX_SATS
            #   nominal_composite / _clamped_price <= MAX_SATS
            # Apply the strictest future price that makes this condition hardest to meet (results in largest expression value)
            #   nominal_composite / low_liquidation_price <= MAX_SATS
            self.nominal_composite / self.low_liquidation_price <= MAX_SATS,

            # Ensure safety requirement [_unsafe_short_sats_out >= 0]
            #   _unsafe_short_sats_out >= 0
            #   nominal_composite / _clamped_price - sats_for_nominal_units_at_high_liquidation >= 0
            # Apply the strictest future price that makes this condition hardest to meet (results in smallest expression value)
            #   nominal_composite / high_liquidation_price - sats_for_nominal_units_at_high_liquidation >= 0
            self.nominal_composite / self.high_liquidation_price - self.sats_for_nominal_units_at_high_liquidation >= 0,

            # Ensure safety requirement [_unsafe_short_sats_out <= MAX_SATS]
            #   _unsafe_short_sats_out <= MAX_SATS
            #   nominal_composite / _clamped_price - sats_for_nominal_units_at_high_liquidation <= MAX_SATS
            # Apply the strictest future price that makes this condition hardest to meet (results in largest expression value)
            #   nominal_composite / low_liquidation_price - sats_for_nominal_units_at_high_liquidation <= MAX_SATS
            # Which is trivially redundant with the same assertion for [_sats_for_nominal_units_at_redemption <= MAX_SATS] which lacks the additional subtraction
            # self.nominal_composite / self.low_liquidation_price - self.sats_for_nominal_units_at_high_liquidation <= MAX_SATS,

            # Ensure safety requirement [_unsafe_long_sats_out >= 0]
            #   _unsafe_long_sats_out >= 0
            #   total_sats_in - short_sats_out >= 0
            #   total_sats_in - max(DUST, _unsafe_short_sats_out) >= 0
            # Apply the strictest choice that makes this condition hardest to meet (results in smallest expression value)
            #   total_sats_in - _unsafe_short_sats_out >= 0
            #   total_sats_in - (nominal_composite / _clamped_price - sats_for_nominal_units_at_high_liquidation) >= 0
            #   total_sats_in - (nominal_composite / _clamped_price) + sats_for_nominal_units_at_high_liquidation >= 0
            # Apply the strictest future price that makes this condition hardest to meet (results in smallest expression value)
            #   total_sats_in - (nominal_composite / low_liquidation_price) + sats_for_nominal_units_at_high_liquidation >= 0
            self.total_sats_in - (self.nominal_composite / self.low_liquidation_price) + self.sats_for_nominal_units_at_high_liquidation >= 0,

            # Ensure safety requirement [_unsafe_long_sats_out <= MAX_SATS]
            #   _unsafe_long_sats_out <= MAX_SATS
            #   total_sats_in - short_sats_out <= MAX_SATS
            #   total_sats_in - max(DUST, _unsafe_short_sats_out) <= MAX_SATS
            # Apply the strictest choice that makes this condition hardest to meet (results in largest expression value)
            #   total_sats_in - DUST <= MAX_SATS
            # Which is trivially redundant with [total_sats_in <= MAX_SATS]
            # self.total_sats_in - DUST <= MAX_SATS,

            # Ensure safety requirement [short_sats_out >= DUST]
            #   min value of short_sats_out >= DUST
            #   min value of max(DUST, _unsafe_short_sats_out) >= DUST
            #   DUST >= DUST (trivially true)

            # Ensure safety requirement [short_sats_out <= MAX_SATS]
            #   max value of short_sats_out <= MAX_SATS
            #   max value of max(DUST, _unsafe_short_sats_out) <= MAX_SATS
            #   max value of _unsafe_short_sats_out <= MAX_SATS (already directly asserted above)

            # Ensure safety requirement [long_sats_out >= DUST]
            #   min value of long_sats_out >= DUST
            #   min value of max(DUST, _unsafe_long_sats_out) >= DUST
            #   DUST >= DUST (trivially true)

            # Ensure safety requirement [long_sats_out <= MAX_SATS]
            #   max value of long_sats_out <= MAX_SATS
            #   max value of max(DUST, _unsafe_long_sats_out) <= MAX_SATS
            #   max value of _unsafe_long_sats_out <= MAX_SATS (already directly asserted above)
        ]

    @property
    def execution_guarantees(self) -> list:
        return [
            # equivalent contract code: clampedPrice = max(min(oraclePrice, highLiquidationPrice), lowLiquidationPrice)
            # https://gitlab.com/GeneralProtocols/anyhedge/contracts/-/blob/development/contracts/v0.12/contract.cash?ref_type=heads#L105
            self._high_clamped_price == If(self.redeem_price < self.high_liquidation_price, self.redeem_price, self.high_liquidation_price),
            self._clamped_price == If(self._high_clamped_price > self.low_liquidation_price, self._high_clamped_price, self.low_liquidation_price),

            # This uses integer mathematics like the BCH VM does, and the result is treated as an integer
            # equivalent contract code: shortSats = max(DUST, (nominalUnitsXSatsPerBch / clampedPrice) - satsForNominalUnitsAtHighLiquidation)
            # https://gitlab.com/GeneralProtocols/anyhedge/contracts/-/blob/development/contracts/v0.12/contract.cash?ref_type=heads#L143
            self._sats_for_nominal_units_at_redemption == self.nominal_composite / self._clamped_price,
            self._unsafe_short_sats_out == self._sats_for_nominal_units_at_redemption - self.sats_for_nominal_units_at_high_liquidation,
            self.short_sats_out == If(self._unsafe_short_sats_out > DUST, self._unsafe_short_sats_out, DUST),

            # equivalent contract code: longSats = max(DUST, payoutSats - shortSats)
            # https://gitlab.com/GeneralProtocols/anyhedge/contracts/-/blob/development/contracts/v0.12/contract.cash?ref_type=heads#L144
            self._unsafe_long_sats_out == self.total_sats_in - self.short_sats_out,
            self.long_sats_out == If(self._unsafe_long_sats_out > DUST, self._unsafe_long_sats_out, DUST),
        ]

    @property
    def safety_requirements(self) -> list:
        return [
            # Contract Parameters
            self.high_liquidation_price >= MIN_PRICE,
            self.high_liquidation_price <= MAX_PRICE,

            self.low_liquidation_price >= MIN_PRICE,
            self.low_liquidation_price <= MAX_PRICE,

            # (Enforce anti-instant liquidation as a safety policy)
            self.low_liquidation_price < self.start_price,
            self.high_liquidation_price > self.start_price,

            self.nominal_composite >= SATS_PER_BCH,
            self.nominal_composite <= VM_MAX_INT,

            self.sats_for_nominal_units_at_high_liquidation >= 0,
            self.sats_for_nominal_units_at_high_liquidation <= MAX_SATS,

            self.total_sats_in >= DUST,
            self.total_sats_in <= MAX_SATS,

            # Execution
            self.redeem_price >= MIN_PRICE,
            self.redeem_price <= MAX_PRICE,

            self._high_clamped_price >= MIN_PRICE,
            self._high_clamped_price <= MAX_PRICE,

            self._clamped_price >= MIN_PRICE,
            self._clamped_price <= MAX_PRICE,

            self._sats_for_nominal_units_at_redemption >= 1,
            self._sats_for_nominal_units_at_redemption <= MAX_SATS,

            self._unsafe_short_sats_out >= 0,
            self._unsafe_short_sats_out <= MAX_SATS,

            self._unsafe_long_sats_out >= 0,
            self._unsafe_long_sats_out <= MAX_SATS,

            # Final Outputs
            self.short_sats_out >= DUST,
            self.short_sats_out <= MAX_SATS,

            self.long_sats_out >= DUST,
            self.long_sats_out <= MAX_SATS,
        ]


@dataclass(frozen=True)
class AnyHedge_v0_12_ByIntent(ContractConstraintsBaseClass):
    """
    The intent approach is not strictly necessary, but lets us provide a user-friendly interface
    when creating contracts that is *also* validation-friendly.
    The approach establishes "intent" constraints possibly together with same base constraints like above
    (because intent constraints can create significant complexity the solver may not be able to handle).
    """
    # Start with the base contract above so that we get all the constants, execution, and safety parts for free.
    base_contract: AnyHedge_v0_12

    # Intent Inputs
    nominal_hedge_units: Real
    low_liquidation_price_multiplier: Real
    high_liquidation_price_multiplier: Real
    is_simple_hedge: Bool

    # Intent to Contract Intermediates
    _sats_for_nominal_units_at_low_liquidation: Int
    _sats_for_nominal_units_at_start: Int
    _short_sats_in: Int
    _long_sats_in: Int

    @classmethod
    def new(cls):
        return cls(
            base_contract=AnyHedge_v0_12.new(),

            # Intent Inputs
            nominal_hedge_units=Real(f'{INTENT_PREFIX}nominal_hedge_units'),
            low_liquidation_price_multiplier=Real(f'{INTENT_PREFIX}low_liquidation_price_multiplier'),
            high_liquidation_price_multiplier=Real(f'{INTENT_PREFIX}high_liquidation_price_multiplier'),
            is_simple_hedge=Bool(f'{INTENT_PREFIX}is_simple_hedge'),

            # Intent to Contract Intermediates
            _sats_for_nominal_units_at_low_liquidation=Int(f'{INTENT_PREFIX}_sats_for_nominal_units_at_low_liquidation'),
            _sats_for_nominal_units_at_start=Int(f'{INTENT_PREFIX}_sats_for_nominal_units_at_start'),
            _short_sats_in=Int(f'{INTENT_PREFIX}_short_sats_in'),
            _long_sats_in=Int(f'{INTENT_PREFIX}_long_sats_in'),
        )

    @property
    def constant_guarantees(self) -> list:
        # We only need the same constants already existing in the original.
        return self.base_contract.constant_guarantees

    @property
    def input_guarantees(self) -> list:
        """
        Establish the boundaries that must be enforced at step 1 of making an intent.
        The concept is that if we can find a set of boundaries here that results in safe
        contracts, then we have a more application-friendly way to be confident about contract safety.

        Note it is critical here that we do not extend the base input guarantees of the parent class.
        For these guarantees to be meaningful, they have to stand alone in ensuring safety.
        """
        # get a short name for readability
        base = self.base_contract

        intent_input_guarantees = [
            # Intent Parameter
            # start_price and redeem_price are on the base, but they are inputs for input guarantee purposes
            # base.start_price > MIN_PRICE,  # redundant with no-instant settlement requirement below
            # base.start_price < MAX_PRICE,  # redundant with no-instant settlement requirement below
            base.redeem_price >= MIN_PRICE,
            base.redeem_price <= MAX_PRICE,

            # Intent Parameter
            # Note: This value can be 1, but using a larger value helps to avoid dangerous and not-useful edges.
            self.nominal_hedge_units >= 1,
            self.nominal_hedge_units <= 10_000_000_00,

            # Intent Parameter
            # Enforce the no-instant settlement by back-calculation here, using a gap of 1 and integer math for simplicity
            # start_price * x <= start_price - 1
            # x <= (start_price - 1) / start_price
            self.low_liquidation_price_multiplier <= (ToReal(base.start_price) - 1) / ToReal(base.start_price),
            self.low_liquidation_price_multiplier >= 0.01,

            # Intent Parameter
            # Enforce the no-instant settlement by back-calculation here, using a gap of 1 and integer math for simplicity
            # start_price * x >= start_price + 1
            # x >= (start_price + 1) / start_price
            self.high_liquidation_price_multiplier >= (ToReal(base.start_price) + 1) / ToReal(base.start_price),
            self.high_liquidation_price_multiplier <= 101,
        ]

        # The intent parameter constraints as currently stated are not sufficient to ensure contract safety.
        # Here some downstream constraints are injected into the set in order to ensure safety.
        # If possible, these should be back-propagated to intent.
        additional_contract_parameter_guarantees = [
            # We drag in an additional value that is cheap to calculate, to avoid non-valuable edges.
            base.low_liquidation_price >= 100,
            base.low_liquidation_price < base.start_price,

            base.high_liquidation_price > base.start_price,
            base.high_liquidation_price <= MAX_PRICE,

            # With easy calculation we can confirm if these are true or not.
            self._short_sats_in >= DUST,
            self._long_sats_in >= DUST,
            base.total_sats_in <= MAX_SATS,

            # Constraints below are remaining constraints from base input guarantees that we didn't eliminate or use yet.

            # base.nominal_composite <= VM_MAX_INT,

            # Ensure safety requirement [_unsafe_short_sats_out >= 0]
            #   min value of _unsafe_short_sats_out >= 0
            #   min value of nominal_composite / _clamped_price >= 0
            #   nominal_composite / high_liquidation_price >= 0
            #   nominal_composite >= 0  (stronger assertion already exists above)

            # Ensure safety requirement [_unsafe_short_sats_out <= MAX_SATS]
            #   max value of _unsafe_short_sats_out <= MAX_SATS
            #   max value of nominal_composite / _clamped_price <= MAX_SATS
            #   nominal_composite / low_liquidation_price <= MAX_SATS
            # self.nominal_composite / self.low_liquidation_price <= MAX_SATS,  # redundant with total sats below

            # Ensure safety requirement [_unsafe_long_sats_out >= 0]
            #   min value of _unsafe_long_sats_out >= 0
            #   min value of total_sats_in - short_sats_out >= 0
            #   min value of total_sats_in - max(DUST, _unsafe_short_sats_out) >= 0
            #   total_sats_in - (max value of max(DUST, _unsafe_short_sats_out)) >= 0
            #   total_sats_in - (max value of _unsafe_short_sats_out) >= 0
            #   total_sats_in - (max value of nominal_composite / _clamped_price) >= 0
            #   total_sats_in - (nominal_composite / low_liquidation_price) >= 0
            # base.total_sats_in - (base.nominal_composite / base.low_liquidation_price) >= 0,
        ]

        combined_input_guarantees = intent_input_guarantees + additional_contract_parameter_guarantees
        return combined_input_guarantees

    @property
    def execution_guarantees(self) -> list:
        """
        It is important to look at and understand clearly what happens here.
        Execution in the base contract is just within the contract.
        However, here, we need to cover the full range from the starting intent,
        through calculations that reach contract parameters, and then through the same
        contract execution that we described before.
        """
        # get a short name for readability
        base = self.base_contract

        intent_to_contract_execution_guarantees = [
            # Contract Parameter
            # Note: Truncating with +0.5 is the integer math equivalent of round_half_up
            base.low_liquidation_price == ToInt(0.5 + (self.low_liquidation_price_multiplier * ToReal(base.start_price))),

            # Contract Parameter
            # Note: This uses real division until the conversion to integer
            # Note: Truncating with +0.5 is the integer math equivalent of round_half_up
            base.high_liquidation_price == ToInt(0.5 + (self.high_liquidation_price_multiplier * ToReal(base.start_price))),

            # Contract Parameter
            # Note: +0.5 is the integer math equivalent of round_half_up
            base.nominal_composite == ToInt(0.5 + self.nominal_hedge_units * ToReal(SATS_PER_BCH)),

            # Contract Parameter
            base.sats_for_nominal_units_at_high_liquidation == If(
                self.is_simple_hedge,
                # simple hedge ==> cost of zero, exact equivalent to original anyhedge
                base.sats_for_nominal_units_at_high_liquidation == 0,
                # leveraged short ==> some cost creating effective leverage
                base.sats_for_nominal_units_at_high_liquidation == base.nominal_composite / base.high_liquidation_price,
            ),

            # Intermediate
            # For orientation, this is equivalent to total sats input for the simple hedge case.
            self._sats_for_nominal_units_at_low_liquidation == base.nominal_composite / base.low_liquidation_price,

            # Contract Parameter
            # Note: Truncating with +0.5 is the integer math equivalent of round_half_up
            base.total_sats_in == self._sats_for_nominal_units_at_low_liquidation - base.sats_for_nominal_units_at_high_liquidation,

            # Intermediate
            self._sats_for_nominal_units_at_start == base.nominal_composite / base.start_price,

            # Intermediate
            self._short_sats_in == self._sats_for_nominal_units_at_start - base.sats_for_nominal_units_at_high_liquidation,

            # Intermediate: long sats in
            self._long_sats_in == base.total_sats_in - self._short_sats_in,
        ]

        # Combine the new intent -> parameters steps above with the existing base contract execution:
        combined_execution_guarantees = intent_to_contract_execution_guarantees + base.execution_guarantees

        return combined_execution_guarantees

    @property
    def safety_requirements(self) -> list:
        """
        Take the existing safety requirements and extend them with safety requirements
        for the new intent and intermediate values. These are important to keep things rational,
        but they are not really critical to safety as long as the base contract safety constraints are included.
        """
        intent_safety_requirements = [
            # Simple positive requirement for nominal hedge safety
            self.nominal_hedge_units >= 1,

            # simple range boundaries for price multipliers
            self.low_liquidation_price_multiplier > 0,
            self.low_liquidation_price_multiplier < 1,
            self.high_liquidation_price_multiplier > 1,


            # simple vm positive integer boundaries for intermediate sat values
            self._sats_for_nominal_units_at_low_liquidation >= 1,
            self._sats_for_nominal_units_at_low_liquidation <= MAX_SATS,

            self._sats_for_nominal_units_at_start >= 1,
            self._sats_for_nominal_units_at_start <= MAX_SATS,

            self._short_sats_in >= 1,
            self._short_sats_in <= MAX_SATS,

            self._long_sats_in >= 1,
            self._long_sats_in <= MAX_SATS,
        ]

        # Combine the new intent -> parameters safety requirements above with the existing base contract safety requirements:
        combined_safety_requirements = self.base_contract.safety_requirements + intent_safety_requirements

        return combined_safety_requirements


##########################
# Main
##########################

# Here we feed our specific model into the generic safety machine to see how we did.

def main():
    print('Checking safety of: AnyHedge v012 base contract parameters')
    describe_safety(
        contract=AnyHedge_v0_12.new(),
        should_describe_constraints=SHOULD_DESCRIBE_CONSTRAINTS,
        should_show_redundancies=SHOULD_SHOW_REDUNDANCIES_FOR_BASE_CONSTRAINTS,
    )

    print('\n\nChecking safety of: AnyHedge v012 intent parameters')
    describe_safety(
        contract=AnyHedge_v0_12_ByIntent.new(),
        should_describe_constraints=SHOULD_DESCRIBE_CONSTRAINTS,
        should_show_redundancies=SHOULD_SHOW_REDUNDANCIES_FOR_INTENT_CONSTRAINTS,
    )


if __name__ == '__main__':
    main()
