# Contract Validators

This repository has the current evolution of tools used to validate safety of the AnyHedge contract.

You can also fork/copy it to adapt for other contracts.
If you make any improvements, bugfixes, etc., it would be great to hear about those or get patches here.

## Where to Start

If you want to get a higher level understanding of what is going on, please start with `./src/common.py`.

## Organization

`./src/` has the common source code used in the current validator scripts. This is the place to start if you want to see the high level structure of how validation is done.

`./validate-*.py` are scripts containing specific implementations of contracts, safety rules and validations.

## Setup

This is not setup as a distributed library or CLI, but just as a standalone, programmable tool.

I highly recommend using a virtual environment of some kind such as [miniconda](https://docs.anaconda.com/free/miniconda/index.html) to setup a python environment separate from your system python. If you ever need to run `sudo` or similar elevated permissions to run something, you're probably doing it wrong/dangerously.

Install requirements:
- `pip install -r requirements.txt` 

## Running validators

See some example results:
- `python validate-anyhedge-v12.py`
